import os
import sys
import timeit

import PIL.Image
import imageio
import skimage
import numpy as np
from tqdm import tqdm

from commons.decorators import profile
import gpu
import cpu

STRATEGY = "cpu"
VIDEO = "futbol.mp4"
SCALE_PERCENT = 50
PATH_ORIGIN_FRAMES = "video/base"
BASE_PATH = "video"
FRAME_FORMAT = ".jpg"
ABSOLUTE_PATH = os.path.abspath("video/")
OUTPUT_VIDEO = "out2.mp4"


@profile(sort_by='cumulative', lines_to_show=10, strip_dirs=True)
def extract_frames():
    print("==== 1. ===")
    print("[STAR] EXTRACT FRAMES")
    reader = imageio.get_reader(VIDEO, 'ffmpeg')
    count = 0
    for i, im in tqdm(enumerate(reader)):
        image = skimage.img_as_float(im).astype(np.float64)
        imageio.imsave(f"{BASE_PATH}/base/{str(i)}{FRAME_FORMAT}", image)
        count = count + 1
    print("[DONE] EXTRACT FRAMES")
    return count


@profile(sort_by='cumulative', lines_to_show=10, strip_dirs=True)
def process_frames(frames=1):
    print("==== 2. ===")
    print("[STAR] APPLY FILTER")
    folder = "scale"
    filter_start = timeit.default_timer()
    for frame in tqdm(range(frames)):
        image = PIL.Image.open(f"{BASE_PATH}/base/{str(frame)}{FRAME_FORMAT}")
        source_array = np.array(image)

        if STRATEGY == "gpu":
            result_array = gpu.grayscale.process(source_array)
        else:
            result_array = cpu.grayscale.process(source_array)

        PIL.Image.fromarray(result_array).save(f"{BASE_PATH}/gray/{str(frame)}{FRAME_FORMAT}")
        try:
            PIL.Image.fromarray(result_array).save(f"{BASE_PATH}/gray/{str(frame)}{FRAME_FORMAT}")
        except OSError as e:
            print(e, file=sys.stderr)
    print("[DONE] APPLY FILTER")
    filter_end = timeit.default_timer()
    print(
        'Tiempo filtro:',
        filter_end - filter_start,
        'sg.'
    )





@profile(sort_by='cumulative', lines_to_show=10, strip_dirs=True)
def convertToVideo():
    print("==== 4. ===")
    print("[STAR] BUILD VIDEO...")
    framerate_video = "30"
    os.system(f"cd {ABSOLUTE_PATH}/gray && ffmpeg -framerate {framerate_video}  -pattern_type glob -i '*{FRAME_FORMAT}' -pix_fmt yuv420p {ABSOLUTE_PATH}/{OUTPUT_VIDEO} -y")

    print("[DONE] BUILD VIDEO")


if __name__ == '__main__':
    program_start = timeit.default_timer()
    process_frames(
        extract_frames()
    )
    convertToVideo()
    program_end = timeit.default_timer()
    print('Tiempo total:', program_end - program_start, 'sg')
