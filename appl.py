import sys
import timeit

import PIL
import numpy as np
from tqdm import tqdm

import cpu
import gpu
import mpi4
from commons.decorators import profile
from video import BASE_PATH, FRAME_FORMAT, STRATEGY


@profile(sort_by='cumulative', lines_to_show=10, strip_dirs=True)
def process_frames(frames=1000):
    print("==== 2. ===")
    print("[STAR] APPLY FILTER")
    folder = "scale"
    filter_start = timeit.default_timer()
    for frame in tqdm(range(frames)):
        image = PIL.Image.open(f"{BASE_PATH}/base/{str(frame)}{FRAME_FORMAT}")
        source_array = np.array(image)

    print("[DONE] APPLY FILTER")
    filter_end = timeit.default_timer()
    print(
        'Tiempo filtro:',
        filter_end - filter_start,
        'sg.'
    )


#process_frames()


process_frames()
