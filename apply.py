import sys
import timeit

import PIL
import numpy as np
from mpi4py import MPI
from tqdm import tqdm

import cpu
import gpu
import mpi4
from commons.decorators import profile
from video import BASE_PATH, FRAME_FORMAT, STRATEGY


@profile(sort_by='cumulative', lines_to_show=10, strip_dirs=True)
def process_frames(frames=100):
    print("==== 2. ===")
    print("[STAR] APPLY FILTER")
    folder = "scale"
    filter_start = timeit.default_timer()
    for frame in tqdm(range(frames)):
        image = PIL.Image.open(f"{BASE_PATH}/base/{str(frame)}{FRAME_FORMAT}")
        source_array = np.array(image)

    print("[DONE] APPLY FILTER")
    filter_end = timeit.default_timer()
    print(
        'Tiempo filtro:',
        filter_end - filter_start,
        'sg.'
    )


#process_frames()


def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type('Enum', (), enums)


tags = enum('READY', 'DONE', 'EXIT', 'START')
comm = MPI.COMM_WORLD
size = comm.size
rank = comm.rank
status = MPI.Status()


if rank == 0:
    tasks = range(2 * size)
    task_index = 0
    num_workers = size - 1
    closed_workers = 0
    print("Master inicia con %d workers" % num_workers)
    while closed_workers < num_workers:
        data = comm.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, status=status)
        source = status.Get_source()
        tag = status.Get_tag()
        if tag == tags.READY:
            # Worker is ready, so send it a task
            if task_index < len(tasks):
                comm.send(tasks[task_index], dest=source, tag=tags.START)
                print("Aplicando filtro a fotogramas tarea %d al worker %d" % (task_index, source))
                task_index += 1
            else:
                comm.send(None, dest=source, tag=tags.EXIT)
        elif tag == tags.DONE:
            results = data
            print("Respuesta desde worker %d" % source)
        elif tag == tags.EXIT:
            print("Worker %d finalizado." % source)
            closed_workers += 1

    print("Proceso Terminado")
else:
    name = MPI.Get_processor_name()
    print("worker %d en instancia %s." % (rank, name))
    while True:
        comm.send(None, dest=0, tag=tags.READY)
        task = comm.recv(source=0, tag=MPI.ANY_TAG, status=status)
        tag = status.Get_tag()

        if tag == tags.START:
            result = task ** 2
            comm.send(result, dest=0, tag=tags.DONE)
        elif tag == tags.EXIT:
            break
    comm.send(None, dest=0, tag=tags.EXIT)

process_frames()