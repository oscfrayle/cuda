import os
import timeit

import imageio
import numpy as np
import skimage
from tqdm import tqdm

from commons.decorators import profile
from video import BASE_PATH, FRAME_FORMAT, VIDEO


@profile(sort_by='cumulative', lines_to_show=10, strip_dirs=True)
def extract_frames():
    print("==== 1. ===")
    print("[STAR] EXTRACT FRAMES")
    reader = imageio.get_reader(VIDEO, 'ffmpeg')
    count = 0
    for i, im in tqdm(enumerate(reader)):
        image = skimage.img_as_float(im).astype(np.float64)
        imageio.imsave(f"{BASE_PATH}/base/{str(i)}{FRAME_FORMAT}", image)
        count = count + 1
    print("[DONE] EXTRACT FRAMES")
    return count


if __name__ == '__main__':
    program_start = timeit.default_timer()
    extract_frames()
    program_end = timeit.default_timer()
    os.system("mpirun -n 4 python apply.py")
    print('Tiempo total:', program_end - program_start, 'sg')
