#!/usr/bin/env python

from __future__ import print_function

import multiprocessing

from mpi4py import MPI
import signal

import numpy as np

from commons import shared

CORES = 8


def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type('Enum', (), enums)


def process(source_array):
    tags = enum('READY', 'DONE', 'EXIT', 'START')

    comm = MPI.COMM_WORLD  # get MPI communicator object
    size = comm.size  # total number of processes
    rank = comm.rank  # rank of this process
    status = MPI.Status()  # get MPI status object

    if rank == 0:
        # Master process executes code below
        tasks = range(2 * size)
        task_index = 0
        num_workers = size - 1
        closed_workers = 0
        while closed_workers < num_workers:
            data = comm.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, status=status)
            source = status.Get_source()
            tag = status.Get_tag()
            if tag == tags.READY:
                # Worker is ready, so send it a task
                if task_index < len(tasks):
                    comm.send(tasks[task_index], dest=source, tag=tags.START)
                    task_index += 1
                else:
                    comm.send(None, dest=source, tag=tags.EXIT)
            elif tag == tags.DONE:
                results = data
            elif tag == tags.EXIT:
                closed_workers += 1
        # print("Master finishing")
        return process_image(source_array)
    else:
        # Worker processes execute code below
        name = MPI.Get_processor_name()
        while True:
            comm.send(None, dest=0, tag=tags.READY)
            task = comm.recv(source=0, tag=MPI.ANY_TAG, status=status)
            tag = status.Get_tag()

            if tag == tags.START:
                # Do the work here
                result = task ** 2
                comm.send(result, dest=0, tag=tags.DONE)
            elif tag == tags.EXIT:
                break

        comm.send(None, dest=0, tag=tags.EXIT)


def process_image(source_array):
    sigint_handler = signal.signal(signal.SIGINT, signal.SIG_IGN)
    raise_sigint = False
    with multiprocessing.Pool(CORES) as pool:
        signal.signal(signal.SIGINT, sigint_handler)
        results = []
        for s in shared.get_segments(source_array):
            start_x, end_x = s[0]
            start_y, end_y = s[1]
            result = pool.apply_async(
                apply_filter,
                (source_array[start_y:end_y, start_x:end_x],)
            )
            results.append((result, (start_x, end_x), (start_y, end_y)))

        result_array = np.empty_like(source_array)
        for r, (start_x, end_x), (start_y, end_y) in results:
            try:
                segment = r.get()
                result_array[start_y:end_y, start_x:end_x] = segment
            except KeyboardInterrupt:
                pool.terminate()
                raise_sigint = True
                break

        pool.close()
        pool.join()

    if raise_sigint:
        raise KeyboardInterrupt

    return result_array


def apply_filter(segment):
    result_array = np.empty_like(segment)
    for i in range(segment.shape[0]):
        for j in range(segment.shape[1]):
            x, y, z = segment[i, j]
            intensity = int(0.2126 * x + 0.7152 * y + 0.0722 * z)
            result_array[i, j] = (intensity,) * 3

    return result_array
