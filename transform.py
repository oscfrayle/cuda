import logging
import os
import boto3
from botocore.exceptions import ClientError

from commons.decorators import profile
from video import ABSOLUTE_PATH, FRAME_FORMAT, OUTPUT_VIDEO


@profile(sort_by='cumulative', lines_to_show=10, strip_dirs=True)
def convertToVideo():
    print("==== 4. ===")
    print("[STAR] BUILD VIDEO...")
    framerate_video = "30"
    os.system(f"cd {ABSOLUTE_PATH}/gray && ffmpeg -framerate {framerate_video}  -pattern_type glob -i '*{FRAME_FORMAT}' -pix_fmt yuv420p {ABSOLUTE_PATH}/{OUTPUT_VIDEO} -y")

    print("[DONE] BUILD VIDEO")


def upload_file(file_name, bucket, object_name=None):
    if object_name is None:
        object_name = os.path.basename(file_name)

    s3_client = boto3.client('s3')
    try:
        response = s3_client.upload_file(file_name, bucket, object_name)
    except ClientError as e:
        logging.error(e)
        return False
    return True
